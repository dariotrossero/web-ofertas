from flask import Flask
import json
import os
import time
import re
import datetime
from flask import render_template
from flask import request

_ip_filter = r"(192|127.0.0.1|172|10)"
app = Flask(__name__)
app.config.from_pyfile('application.cfg',
                       silent=True)


def save_ip(ip, resource):
    f = open("ips.txt", "a+")
    f.write(datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S") + ',' + ip + "," + resource + "\n")
    f.close()


@app.route('/')
def hello():
    return "Hello"


def see_ips():
    matrix = []
    exists = os.path.exists("ips.txt")
    if exists:
        with open("ips.txt", "r") as f:
            lines = f.readlines()
            for line in lines[::-1]:
                a = line.split(',')
                matrix.append({"date": a[0], "address": a[1], "resource": a[2]})
        return matrix
    else:
        return []


@app.route('/<shop>/<category>')
def offer(shop, category):
    shop = shop.lower()
    category = category.lower()
    elements = []
    filename = '{0}/{1}_{2}.json'.format(app.config['OFFERS_FOLDER'], shop.upper(), category.capitalize())
    with open(filename) as json_file:
        data = json.load(json_file)
        columns = 4
        products = [data[x:x + columns] for x in range(0, len(data), columns)]
        filename_lu = os.path.getmtime(filename)
        last_update = time.strftime('%d de %b %H:%M', time.localtime(filename_lu))
        elements.append({"items": products, "last_update": last_update})
        return render_template('{0}.html'.format(shop), elements=elements)


@app.route('/productos/<category>')
def offers_by_category(category):
    category = category.lower()
    elements = []
    shops = ['COOPERATIVA', 'VEA', 'WALMART']
    for i in shops:
        filename = '{0}/{1}_{2}.json'.format(app.config['OFFERS_FOLDER'], i, category.capitalize())
        with open(filename) as json_file:
            data = json.load(json_file)
            for x in data:
                x['shop'] = i
            elements= elements+ data
    columns = 4
    elements = sorted(elements, key=lambda i: i['marca'])
    products = [elements[x:x + columns] for x in range(0, len(elements), columns)]
    return render_template('products.html', elements=products)

@app.route('/<shop>')
def offers_by_shop(shop):
    if 'favicon.ico' == shop.lower():
        return ''
    else:
        if not re.match(_ip_filter, request.remote_addr):
            save_ip(request.remote_addr, shop)
        shop = shop.lower()
        elements = []
        files = os.listdir(app.config['OFFERS_FOLDER'])
        categories = [k for k in files if shop.upper() in k]
        for i in categories:
            filename = '{0}/{1}'.format(app.config['OFFERS_FOLDER'], i)
            with open(filename) as json_file:
                data = json.load(json_file)
                columns = 4
                products = [data[x:x + columns] for x in range(0, len(data), columns)]
                filename_lu = os.path.getmtime(filename)
                last_update = time.strftime('%d de %b %H:%M', time.localtime(filename_lu))
                if len(products) == 0:
                    continue
                elements.append({"items": products, "last_update": last_update})
        return render_template('{0}.html'.format(shop), elements=elements)


@app.route('/stats/ips')
def ips():
    ip_list = see_ips()
    return render_template('stats_ips.html', elements=ip_list)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
